import os
import sys
import hashlib


os.makedirs("split-image")


path = sys.argv[1]

hsh = hashlib.sha256()
part = b""

manifest = []

last_update = -1

file_size = os.stat(DEST_PATH).st_size

with open(path, "rb") as f:
    while True:
        update = 100 * f.tell() // file_size
        if update != last_update:
            print(f"{update}% done")
            last_update = update

        block = f.read(4096 * 8)
        if not block:
            break
        part += block
        hsh.update(block)

        if hsh.digest()[:1] == b"\x00":
            hsh_s = hsh.hexdigest()[:32]
            manifest.append(hsh_s)
            with open(f"split-image/{hsh_s}", "wb") as f1:
                f1.write(part)
            hsh = hashlib.sha256()
            part = b""

with open("split-image/manifest", "w") as f1:
    f1.write("\n".join(manifest) + "\n")
