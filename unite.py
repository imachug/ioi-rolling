import os
import sys
import hashlib
import urllib.request


URL_BASE = "https://raw.githubusercontent.com/imachug/ioi-rolling/master/split-image"
DEST_PATH = "C:\\Users\\user\\VirtualBox VMs\\vm\\ioi-2021.0-disk1"

bak = 1
while os.path.exists(f"{DEST_PATH}.bak-{bak}"):
    bak += 1
bak_path = f"{DEST_PATH}.bak-{bak}"
os.rename(DEST_PATH, bak_path)
print("Backed up the image to", bak_path)

known_hashes = {}

hsh = hashlib.sha256()
part_start = 0

with open(bak_path, "rb") as f:
    while True:
        block = f.read(4096 * 8)
        if not block:
            break
        hsh.update(block)

        if hsh.digest()[:1] == b"\x00":
            hsh_s = hsh.hexdigest()[:32]
            known_hashes[hsh_s] = (part_start, f.tell() - part_start)
            hsh = hashlib.sha256()
            part_start = f.tell()

    with urllib.request.urlopen(f"{URL_BASE}/manifest") as f_man:
        manifest = f_man.read().decode().split()

    cnt_hashes_to_download = len(hsh_s for hsh_s in manifest if hsh_s not in known_hashes)
    print("Pieces to download:", cnt_hashes_to_download)

    with open(DEST_PATH, "wb") as f_dst:
        for hsh_s in f_man:
            hsh_s = hsh_s.strip()
            if not hsh_s:
                continue
            if hsh_s in known_hashes:
                f.seek(known_hashes[hsh_s][0])
                f_dst.write(f.read(known_hashes[hsh_s][1]))
            else:
                print("Downloading piece", hsh_s)
                url = f"{URL_BASE}/{hsh_s}"
                with urllib.request.urlopen(url) as f_piece:
                    f_dst.write(f_piece.read())
